package eu.mvanco.camera;

import java.io.File;

public abstract class AlbumStorageDirFactory { //some changes
	public abstract File getAlbumStorageDir(String albumName);
}
